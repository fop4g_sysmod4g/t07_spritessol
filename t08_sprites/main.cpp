#include <assert.h>
#include <string>

#include "SFML/Graphics.hpp"


using namespace sf;
using namespace std;

/*
* Solution file for loading and rendering sprites with SFML
*/

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};


/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width,
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 800,600 };

	const char ESCAPE_KEY{ 27 };
}

bool LoadTexture(const string& file, Texture& tex)
{
	if (tex.loadFromFile(file))
	{
		tex.setSmooth(true);
		return true;
	}
	assert(false);
	return false;
}


int main()
{
	// Create the main window
	RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "My first code");

	Texture armyTex;
	LoadTexture("data/2dsprite.png", armyTex);

	Texture ramTex;
	LoadTexture("data/rampage.png", ramTex);


	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed) 
				window.close();
			if (event.type == Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close(); 
			}
		} 

		// Clear screen
		window.clear();

		Sprite spr(armyTex);
		Vector2u screenSz = window.getSize();
		spr.setPosition(screenSz.x / 2.f, screenSz.y / 2.f);
		IntRect texR = spr.getTextureRect();
		spr.setOrigin(texR.width / 2.f, texR.height / 2.f);
		spr.setScale(0.25f, 0.25f);
		window.draw(spr);

		Sprite spr2(ramTex);
		spr2.setScale(0.25f,0.25f);
		const float space = 50, xOff = 30, yOff = 30;
		for(int y=0;y<5;++y)
			for (int x = 0; x < 5; ++x)
			{
				spr2.setPosition(x*space + xOff, y*space + yOff);
				window.draw(spr2);
			}

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
